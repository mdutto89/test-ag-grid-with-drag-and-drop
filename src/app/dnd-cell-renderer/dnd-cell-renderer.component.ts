import {Component} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {IAfterGuiAttachedParams, ICellRendererParams} from 'ag-grid';

@Component({
  selector: 'app-dnd-cell-renderer',
  templateUrl: './dnd-cell-renderer.component.html',
  styleUrls: ['./dnd-cell-renderer.component.css']
})
export class DndCellRendererComponent implements ICellRendererAngularComp {

  public data: object;

  afterGuiAttached(params?: IAfterGuiAttachedParams): void {
  }

  agInit(params: ICellRendererParams): void {
    this.data = params.node.data;
  }

  refresh(params: any): boolean {
    return false;
  }
}
