import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AgGridModule} from 'ag-grid-angular';
import {DndModule} from 'ng2-dnd';
import {FormsModule} from '@angular/forms';
import {DndCellRendererComponent} from './dnd-cell-renderer/dnd-cell-renderer.component';

@NgModule({
  declarations: [
    AppComponent,
    DndCellRendererComponent
  ],
  imports: [
    BrowserModule,
    AgGridModule.withComponents([DndCellRendererComponent]),
    DndModule.forRoot(),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
