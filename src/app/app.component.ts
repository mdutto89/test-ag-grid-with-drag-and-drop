import {Component} from '@angular/core';
import {DragDropData} from 'ng2-dnd';
import {DndCellRendererComponent} from './dnd-cell-renderer/dnd-cell-renderer.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  columnDefs = [
    {headerName: 'Fruit', field: 'fruit'},
    {
      headerName: 'Color', field: 'color', cellRenderer: 'dndCellRenderer', cellStyle: {
        'background-color': 'lightskyblue'
      }
    },
    {headerName: 'Season', field: 'season'}
  ];

  rowData = [
    {fruit: 'Strawberry', color: 'Red', season: 'Summer'},
    {fruit: 'Banana', color: 'Yellow', season: 'Any'},
    {fruit: 'Kiwi', color: 'Green', season: 'Winter'}
  ];

  fruitColor: string;
  frameworkComponents = {
    dndCellRenderer: DndCellRendererComponent
  };
  fruitName: string;
  class: string;

  setColorValue($event: DragDropData) {
    this.fruitColor = $event.dragData.color;
    this.class = this.fruitColor.toLowerCase();
  }

  setNameValue($event: DragDropData) {
    this.fruitName = $event.dragData.fruit;
  }

  openWindow() {
    window.open(window.location.href);
  }
}
